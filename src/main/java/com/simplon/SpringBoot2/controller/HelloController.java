package com.simplon.SpringBoot2.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
public class HelloController {

    @GetMapping("/helloWorld")
    @ResponseBody
    public String sayHello() {

        return "Hello world";
    }

    @GetMapping("/helloTo")
    @ResponseBody
    public String sayHelloTo(@RequestParam(required = false, defaultValue = "Simplon") String name) {

        return "Hello " + name;
    }
}
